package com.yth.mastermoviles.personalizacioncomponentes;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import java.util.Random;

/**
 * Created by mastermoviles on 24/10/17.
 */

public class TextViewCitas extends android.support.v7.widget.AppCompatTextView {

    private String[] citas = {
            "No se puede poseer mayor gobierno, ni menor, que el de uno mismo",
            "Abrid escuelas y se cerrarán cárceles",
            "No hemos sido los primeros, pero seremos los mejores",
            "Es mejor morir de pie que vivir arrodillado",
            "El dinero no puede comprar la vida",
            "Lo que no te mata, te hace más fuerte",
            "El sabio no dice todo lo que piensa, pero siempre piensa todo lo que dice"
    };

    public TextViewCitas(Context context) {
        super(context);
        changeCita();
    }

    public TextViewCitas(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        changeCita();
    }

    public TextViewCitas(Context context, AttributeSet attrs) {
        super(context, attrs);
        changeCita();
    }

    public void changeCita() {
        String nuevo = citas[new Random().nextInt(citas.length)];
        if (nuevo.equals(this.getText().toString())) {
            changeCita();
        } else {
            this.setText(nuevo);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        changeCita();
        return super.onTouchEvent(event);
    }

}
