package com.yth.mastermoviles.personalizacioncomponentes;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by mastermoviles on 24/10/17.
 */

public class Grafica extends View {

    private float porcentaje = 25;
    private RectF oval = new RectF();
    private static int DEFAULT_SIZE = 200;

    public Grafica(Context context) {
        super(context);
    }

    public Grafica(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        getPorcentaje(attrs);
    }

    public Grafica(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getPorcentaje(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Grafica(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        getPorcentaje(attrs);
    }

    private void getPorcentaje(AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray ta = this.getContext().obtainStyledAttributes(attrs, R.styleable.Grafica);
        porcentaje = ta.getInt(R.styleable.Grafica_porcentaje, 0);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        Paint relleno = new Paint();
        relleno.setStyle(Paint.Style.FILL);
        int colorAccent = ContextCompat.getColor(getContext(), R.color.colorAccent);
        relleno.setColor(colorAccent);

        Paint resto = new Paint();
        resto.setStyle(Paint.Style.FILL);
        int colorPrimary = ContextCompat.getColor(getContext(), R.color.colorPrimary);
        resto.setColor(colorPrimary);

        float angulo = (float) (porcentaje*360.0/100.0);

        canvas.drawArc(oval, 0, angulo, true, relleno);
        canvas.drawArc(oval, angulo, 360-angulo, true, resto);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width = DEFAULT_SIZE;
        int height = DEFAULT_SIZE;

        switch (widthMode) {
            case MeasureSpec.EXACTLY:
                width = widthSize;
                break;
            case MeasureSpec.AT_MOST:
                //if(width > widthSize)
                width = widthSize;
                break;
        }

        switch (heightMode) {
            case MeasureSpec.EXACTLY:
                height = heightSize;
                break;
            case MeasureSpec.AT_MOST:
                //if (height > heightSize)
                height = heightSize;
                break;
        }

        int min = Math.min(width, height);
        width = min;
        height = min;

        //oval = new RectF(0 , 0, width/2 + Math.min(width,height) , height/2 + Math.min(width, height));
        oval = new RectF(0 , 0, width, height);

        this.setMeasuredDimension(width,height);
    }

    public void setPorcentaje(int perc) {
        porcentaje = perc;
        invalidate();
    }
}
