package com.yth.mastermoviles.personalizacioncomponentes;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by mastermoviles on 24/10/17.
 */

public class EdicionBorrable extends LinearLayout {

    EditText edit_text;
    Button edit_button;

    public EdicionBorrable(Context context) {
        super(context);
        setLayout();
    }

    public EdicionBorrable(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setLayout();
    }

    public EdicionBorrable(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setLayout();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EdicionBorrable(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setLayout();
    }

    private void setLayout() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater)getContext().getSystemService(infService);
        li.inflate(R.layout.edicion_borrable_layout, this, true);

        edit_text = (EditText)findViewById(R.id.edit_text);
        edit_button = (Button)findViewById(R.id.edit_button);

        edit_button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                edit_text.setText("");
            }
        });
    }


}
